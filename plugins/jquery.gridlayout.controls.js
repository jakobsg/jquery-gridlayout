
;(function($, window, document, undefined) {

	var fn = $.GridLayout;

	fn.add_control_panel = function(){
		var self = this
		this.$control_panel = $('<div class="layout-control-panel"></div>');
		this.$viewport.append(this.$control_panel);
		this.$control_panel.click( function() {
			self.toggle_widget_navigation();
		});
		this.$control_panel.hover(
			function() {
				$(this).stop();
				$(this).animate({right:-1},{
					duration: 200,
					easing: "swing"
				});
			},
			function() {
				$(this).stop();
				$(this).animate({right:-50},{
					duration: 200,
					easing: "swing",
				});
			}
		);
	}

}(jQuery, window, document));


