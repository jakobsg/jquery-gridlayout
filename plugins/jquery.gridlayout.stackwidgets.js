;(function($, window, document, undefined) {
	
	var fn = $.GridLayout;
	
	fn.update_stack_widget_controls = function(target) {
		var self = this;
		var controls = $('<div class="controls"></div>');
		var stackcount = 0;
		var target_frame = target.children("div.frame");
		target_frame.children("div.content").each( function() {
			var name = $(this).attr('name');
			var title = $(this).attr('title');
			if (!name && title) {
				name = title.toLowerCase();
				$(this).attr('name',name);
			}
			if (!title && name) {
				title = name;
				$(this).attr('title',title);
			}
			if (!name && !title) {
				name = "tab-" + self.$name_idx;
				title = "Untitled " + self.$name_idx;
				self.$name_idx++;
			}
			controls.append('<div class="item" name="'+name+'">'+title+'</div>');
			stackcount++;
		});
		if (stackcount) {
			target.addClass("tab");
		}
		else {
			target.removeClass("tab");
		}
		target_frame.children('div.controls').remove();
		var cur_content_name = target_frame.children("div.content:visible").attr('name');
		if (cur_content_name!=undefined) {
			controls.children('div.item[name='+cur_content_name+']').addClass('active');
		}
		target_frame.prepend(controls);
		target_frame.find("div.controls div.item").unbind('click');
		target_frame.find("div.controls div.item").click( function() {
			var n = $(this).attr("name");
			$(this).siblings().removeClass("active");
			$(this).addClass("active");
			target_frame.children('div.content[name!='+n+']').fadeOut();
			target_frame.children('div.content[name='+n+']').fadeIn();
			self.trigger_widget_resize(target);
		});
	}
	
	fn.enable_stack_widgets = function(enable) {
		var self = this;
		if (typeof(self.$name_idx)==undefined) {
			self.$name_idx = 0;
		}
		if (enable) {
			if (!this.$viewport.children("style[name=stack-widget-style]").length) {
				var stackwidget_style = $('<style name="stack-widget-style"></style>');
				stackwidget_style.append('div.' + this.$classid + ' div.widget.stacked.tab div.frame div.content { top: ' + (this.$wdg_margins+37) + 'px; left: ' + this.$wdg_margins + 'px}');
				stackwidget_style.append("div." + this.$classid + " div.widget.stacked.tab div.frame div.content { height: calc(100% - "+(this.$wdg_margins*2+37)+"px); width: calc(100% - "+(this.$wdg_margins*2)+"px)}");
				stackwidget_style.append('div.' + this.$classid + ' div.widget.stacked.tab div.frame div.controls { position: relative;float: left;height: 37px;width: calc(100%-'+this.$wdg_margins*2+'px);left: '+ this.$wdg_margins +'px;top: '+ this.$wdg_margins +'px;}');
				this.$viewport.prepend(stackwidget_style);
			}
			this.$widgets.addClass("stacked");
			this.$widgets.each( function() {
				if ($(this).children("div.frame").children("div.content").length>1) {
					self.update_stack_widget_controls($(this));
				}
			});
			this.$widgets.unbind("dropped-on.stackwidgets");
			this.$widgets.bind("dropped-on.stackwidgets",function(e,ui) {
				var dragged_content = $(this).children("div.frame").children("div.content").detach();
				dragged_content.show();
				ui.target.children("div.frame").children("div.content").hide();
				ui.target.children("div.frame").children('div.content:last').after(dragged_content);
				self.update_stack_widget_controls(ui.target);
				$(this).remove();
				self.trigger_widget_resize(ui.target);
			})
			self.trigger_widget_resize(self.$widgets);
		}
		else {
			this.$widgets.removeClass("stacked");
			this.$widgets.children("div.frame").children('div.controls').remove();
			this.$widgets.unbind("dropped-on.stackwidgets");
			self.trigger_widget_resize(self.$widgets);
			this.$viewport.children("style[name=stack-widget-style]");
		}
	}

}(jQuery, window, document));


