/*
 * jquery.layout
 *
 * Copyright (c) 2013 Jakob Simon-Gaarde
 * Licensed under the MIT licenses.
 */
;(function($, window, document, undefined) {

	var defaults = {
		grid_resolution: 40,
		widget_margins: 6,
		widget_spacing: 8
	};
	
	var gl_idx = 1;

	/**
	* @class GridLayout
	* @param {HTMLElement} viewport The HTMLelement that contains all the widgets.
	* @param {Object} [options] An Object with all options you want to
	*        overwrite:
	*    @param {HTMLElement|String} [options.widget_selector] Define who will
	*     be the draggable widgets. Can be a CSS Selector String or a
	*     collection of HTMLElements
	*    @param {Array} [options.widget_margins] Margin between widgets.
	*     The first index for the horizontal margin (left, right) and
	*     the second for the vertical margin (top, bottom).
	*    @param {Array} [options.widget_base_dimensions] Base widget dimensions
	*     in pixels. The first index for the width and the second for the
	*     height.
	*    @param {Number} [options.extra_cols] Add more columns in addition to
	*     those that have been calculated.
	*
	* @constructor
	*/
	function GridLayout(viewport, options) {
		this.options = $.extend(true, defaults, options);
		this.$viewport = $(viewport);
		this.$classid = "grid-layout-"+gl_idx++;
		this.$wdg_margins = this.options.widget_margins;
		this.$wdg_spacing = this.options.widget_spacing;
		this.$viewport.addClass(this.$classid);
		this.$viewport.append("<style>div." + this.$classid + " div.widget div.frame { top: "+this.$wdg_spacing+"px; left: "+this.$wdg_spacing+"px} </style>");
		this.$viewport.append("<style>div." + this.$classid + " div.widget div.frame { height: calc(100% - "+(this.$wdg_spacing*2)+"px); width: calc(100% - "+(this.$wdg_spacing*2)+"px)} </style>");
		this.$viewport.append("<style>div." + this.$classid + " div.widget div.content { top: "+this.$wdg_margins+"px; left: "+this.$wdg_margins+"px} </style>");
		this.$viewport.append("<style>div." + this.$classid + " div.widget div.content { height: calc(100% - "+(this.$wdg_margins*2)+"px); width: calc(100% - "+(this.$wdg_margins*2)+"px)} </style>");
		this.$widgets = this.$viewport.find("div.widget");
		this.$grid_res = this.options.grid_resolution;
		this.$widget_navigation_enabled = false;
		this.$cur_viewport_width = this.$viewport.width();
		this.$cur_viewport_height = this.$viewport.height();
		this.$cur_grid_width = this.$cur_viewport_width/this.$grid_res;
		this.$cur_grid_height = this.$cur_viewport_height/this.$grid_res;
		this.init();
	}

	var fn = GridLayout.prototype;

	fn.init = function() {
		var self = this;
		this.$widgets.each( function() {
			$(this).children("div.frame").children('div.content:first').show();
		});
		$(window).resize(function(e) {
			self.resize_handler();
		});
		this.resize_handler();
	};

	fn.update_widget_geometry = function(elems) {
		var self = this;
		$(elems).each( function() {
			var elem = $(this)
			var x = parseInt(elem.attr('x'));
			var y = parseInt(elem.attr('y'));
			var w = parseInt(elem.attr('w'));
			var h = parseInt(elem.attr('h'));
			if (!isNaN(x)) {
				elem.css("left", x*(100/self.$grid_res)+"%");
			}
			if (!isNaN(y)) {
				elem.css("top", y*(100/self.$grid_res)+"%");
			}
			if (!isNaN(w)) {
				elem.css("width", w*(100/self.$grid_res)+"%");
			}
			if (!isNaN(h)) {
				elem.css("height", h*(100/self.$grid_res)+"%");
			}
		});
	}

	fn.trigger_widget_resize = function(objs,options) {
		var self = this;
		var opt = $.extend({widthResized:true, heightResized:true, widgetSize: null},options);
		var widget_size = opt.size;
		
		$(objs).each( function() {
			var obj = $(this);
			var non_content_height = self._non_content_height(obj);
			if (!widget_size) {
				widget_size = {
					width: obj.width(),
					height: obj.height()
				};
			}
			var content_size = {
				width: obj.children("div.frame").children("div.content:first").width(),
				height: obj.children("div.frame").children("div.content:first").height()
			};
			//obj.children("div.content").width(content_size.width);
			//obj.children("div.content").height(content_size.height);
			if (opt.widthResized) {
				obj.trigger("resize-width",{
					widgetSize: widget_size,
					contentSize: content_size
				});
			}
			if (opt.heightResized) {
				obj.trigger("resize-height",{
					widgetSize: widget_size,
					contentSize: content_size
				});
			}
		});
	}

	fn.resize_handler = function() {
		var height_change = false;
		var width_change = false;
		if (this.$cur_viewport_width!=this.$viewport.width()) {
			width_change = true;
		} 
		if (this.$cur_viewport_height!=this.$viewport.height()) {
			height_change = true;
		}
		if (!width_change && !height_change) {
			return;
		}
		var self = this;
		this.$cur_viewport_width = this.$viewport.width();
		this.$cur_viewport_height = this.$viewport.height();
		this.$cur_grid_width = this.$cur_viewport_width/this.$grid_res;
		this.$cur_grid_height = this.$cur_viewport_height/this.$grid_res;
		self.trigger_widget_resize(this.$widgets,{ widthResized: width_change, heightResized: height_change });
		
		var resizable_widgets = this.$viewport.find('.widget.ui-resizable');
		resizable_widgets.resizable("destroy");
		var draggable_widgets = this.$viewport.find('.widget.ui-draggable');
		draggable_widgets.draggable("destroy");
		var droppable_widgets = this.$viewport.find('.widget.ui-droppable');
		droppable_widgets.droppable("destroy");
		
		if (this.$widget_navigation_enabled) {
			resizable_widgets.each( function() {
				self.set_widget_resizable($(this),true);
			});
			draggable_widgets.each( function() {
				self.set_widget_draggable($(this),true);
			});
			droppable_widgets.each( function() {
				self.set_widget_droppable($(this),true);
			});
		}
	}
	
	fn.set_widget_droppable = function(obj,enable) {
		if (enable) {
			var self = this;
			if (obj.hasClass("ui-droppable")) {
				return;
			}
			obj.droppable({
				accept: this.$viewport.find("div.widget"),
				drop: function( event, ui ) {
					var zindex = parseInt(obj.css('z-index'));
					if (zindex > parseInt(ui.draggable.prop('max-drop-z-index'))) {
						ui.draggable.prop('max-drop-z-index',zindex);
						ui.draggable.prop('dropped-on',obj);
					}
				},
				tolerance: "pointer"
			});
		}
		else {
			if (obj.hasClass("ui-droppable")) {
				obj.droppable("destroy");
			}
		}
	}
	
	fn.set_widget_draggable = function(obj,enable) {
		if (enable) {
			var self = this;
			if (obj.hasClass("ui-draggable")) {
				return;
			}
			obj.draggable({
				grid: [this.$cur_grid_width-.1,this.$cur_grid_height-.1],
				containment: this.$viewport,
				
				stack: this.$viewport.find("div.widget"),
				start: function() {
					obj.prop('dropped-on',null);
					obj.prop('max-drop-z-index',-1);
				},
				stop: function (event,ui) {
					var elmObj = $(this);
					var x = Math.round(elmObj.position().left/self.$cur_grid_width);
					var y = Math.round(elmObj.position().top/self.$cur_grid_height);
					elmObj.attr('x',x);
					elmObj.attr('y',y);
					self.update_widget_geometry(elmObj);
					if (elmObj.prop('dropped-on')) {
						elmObj.trigger("dropped-on", {
							target: elmObj.prop('dropped-on'),
							self: elmObj
						});
					}
				}
			});
		}
		else {
			if (obj.hasClass("ui-draggable")) {
				obj.draggable("destroy");
			}
		}
	}
	
	fn._non_content_height = function(obj) {
		var non_content_height = 0;
		obj.children("div.frame").children(":not(div.content):not(div.navigation-overlay):not(div.ui-resizable-handle)").each( function() {
			non_content_height += $(this).height();
		});
		return non_content_height;
	}
	
	fn.set_widget_resizable = function(obj,enable) {
		var self = this;
		if (enable) {
			if (obj.hasClass("ui-resizable")) {
				return;
			}
			obj.resizable({
				grid: [this.$cur_grid_width,this.$cur_grid_height],
				animate: false,
				minWidth: this.$cur_grid_width,
				minHeight: this.$cur_grid_height,
				containment: this.$viewport,
				autoHide: true,
				handles: "all",
				start: function ( event, ui ) {
					obj.prop('previousSize',$.extend({},ui.size));
				},
				resize: function( event, ui ) {
					var prev_size = obj.prop('previousSize');
					self.trigger_widget_resize(this,{
						widthResized: ui.size.width!=prev_size.width,
						heightResized: ui.size.height!=prev_size.height,
						widgetSize: ui.size
					});
					obj.prop('previousSize',$.extend({},ui.size));
				},
				stop: function (event,ui) {
					obj.prop('previousSize',null);
					var elmObj = $(this);
					var w = Math.round(elmObj.width()/self.$cur_grid_width);
					var h = Math.round(elmObj.height()/self.$cur_grid_height);
					var x = Math.round(elmObj.position().left/self.$cur_grid_width);
					var y = Math.round(elmObj.position().top/self.$cur_grid_height);
					elmObj.attr('x',x);
					elmObj.attr('y',y);
					elmObj.attr('w',w);
					elmObj.attr('h',h);
					self.update_widget_geometry(elmObj);
				}
			});
		}
		else {
			if (obj.hasClass("ui-resizable")) {
				obj.resizable("destroy");
			}
		}
	}

	fn.set_widget_navigation_mode = function(obj,enable) {
		obj = $(obj);
		if (enable) {
			var nav_mode = obj.attr("nav_mode");
			if (typeof(nav_mode)!="undefined" && nav_mode === "1") {
				return;
			}
			obj.attr("nav_mode","1");
			obj.addClass("resize-mode");
			var widget_overlay = $('<div class="navigation-overlay"><div class="draggable-icon"></div></div>');
			obj.append(widget_overlay);
			widget_overlay.children('div.draggable-icon').hide();
			obj.hover(
				function() {
					$(this).find("div.navigation-overlay div.draggable-icon").show()
				},
				function() {
					$(this).find("div.navigation-overlay div.draggable-icon").hide()
				}
			);
			this.set_widget_resizable(obj,enable);
			this.set_widget_draggable(obj,enable);
			this.set_widget_droppable(obj,enable);
		}
		else {
			obj.unbind("hover");
			var self = this;
			obj.find("div.navigation-overlay").fadeOut(500,function() {
				obj.find("div.navigation-overlay").remove();
			});
			obj.removeClass("resize-mode");
			obj.attr("nav_mode","0");
			if (obj.hasClass('ui-resizable')) {
				obj.resizable("destroy");
			}
			if (obj.hasClass('ui-draggable')) {
				obj.draggable("destroy");
			}
			this.set_widget_resizable(obj,enable);
			this.set_widget_draggable(obj,enable);
			this.set_widget_droppable(obj,enable);
		}
	}
	
	fn.enable_widget_navigation = function(enable) {
		var self = this;
		if (enable == this.$widget_navigation_enabled) {
			return;
		}
		this.$widget_navigation_enabled = enable;
		if (enable) {
			// Create the navigation grid
			var grid_overlay = $('<div class="grid overlay fill-parent"></div>');
			this.$viewport.prepend(grid_overlay);
			for (var x=0; x<this.$grid_res; x++) {
				for (var y=0; y<this.$grid_res; y++) {
					var gridcell = $('<div class="grid-cell"></div>');
					gridcell.css("left", x*(100/this.$grid_res)+"%");
					gridcell.css("top", y*(100/this.$grid_res)+"%");
					gridcell.css("width", (100/this.$grid_res)+"%");
					gridcell.css("height", (100/this.$grid_res)+"%");
					grid_overlay.append(gridcell);
				}
			}
			grid_overlay.fadeIn(500);
			// Put all widgets in navigation mode
			this.$viewport.find("div.widget").each( function() {
				self.set_widget_navigation_mode(this,true);
			});
			$(document).bind("keyup.navigation_bindings",function(e){
				if(e.keyCode === 27) {
					self.enable_widget_navigation(false);
					$(document).unbind("keyup.navigation_bindings");
				}
			});
		}
		else {
			this.$viewport.find("div.widget").each( function() {
				self.set_widget_navigation_mode(this,false);
			});
			this.$viewport.find("div.grid.overlay").fadeOut(500, function() {
				self.$viewport.find("div.grid.overlay").remove();
			});
		}
	}

	fn.toggle_widget_navigation = function() {
		this.enable_widget_navigation(!this.$widget_navigation_enabled);
	}

    //jQuery adapter
	$.fn.gridlayout = function(options) {
		return this.each(function() {
			if (!$(this).data('gridlayout')) {
				$(this).data('gridlayout', new GridLayout( this, options ));
			}
		});
	};

    $.GridLayout = fn;

}(jQuery, window, document));
