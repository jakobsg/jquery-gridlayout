# README #

With jquery-gridlayout you get full control of the browser window rather than the document. Instead of a long
scrolling web page you get more of an application style layout which can be preferred in many cases.

Webmail clients is a well known application group where this type of layout behaviour is preferred. Se for
instance:

Mail clients:

* [Zimbra](https://www.zimbra.com/)
* [Roundcube](https://roundcube.net/screens)

### jquery-gridlayout ###

* jquery-gridlayout gives you an absolute layout of your divs with no scrolling page.
  This is great if you are developing a desktop-application like webapp.
* View jquery-gridlayout in action: [Demo](https://jakobsg.bitbucket.io/jquery-gridlayout-demo/demo/index.html)

### How do I get set up? ###

* To get jquery-gridlayout on your site either:
** Clone the repository: git clone https://bitbucket.org/jakobsg/jquery-gridlayout.git
** or use bower: bower install jquery-gridlayout

### Usage ###

* Go to: [Get started](https://jakobsg.bitbucket.io/jquery-gridlayout-demo/index.html)
